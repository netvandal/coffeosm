import base64
import traceback
import cv2
from PIL import Image
import pytesseract
import requests
import spacy
from postal.parser import parse_address
from receiptProcessor import ReceiptProcessor
import numpy as np
from io import BytesIO
from OSMPythonTools.nominatim import Nominatim
import overpy
import json 
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

import tempfile
import os

import cv2
import numpy as np
from PIL import Image

class GCVisionProcessor(ReceiptProcessor):
    def __init__(self):
        self.nlp = spacy.load("en_core_web_sm")
        self.nominatim = Nominatim()
        self.overpassapi = overpy.Overpass()
        self.api_key = "" #TODO: add your own API key

    def extract_data(self, image_path: str):
        preprocessed_image = self.preprocess_image(image_path)
        text = self.extract_text(preprocessed_image)
        data = self.guess_data(text)  
        return data

    def preprocess_image(self, image_path: str) -> bytes:
        # Convert the image from bytes to a numpy array
        with open(image_path, 'rb') as f:
            image_bytes = f.read()
        img = np.frombuffer(image_bytes, np.uint8)
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Convert the image back to bytes
        ret, gray = cv2.imencode('.jpg', gray)
        return gray.tobytes()

    def extract_text(self, image: bytes) -> str:
        # Encode the image as base64
        image_content = base64.b64encode(image).decode('utf-8')

        # Send a POST request to the Vision API
        url = f'https://vision.googleapis.com/v1/images:annotate?key={self.api_key}'
        data = {
            'requests': [{
                'image': {
                    'content': image_content
                },
                'features': [{
                    'type': 'DOCUMENT_TEXT_DETECTION'
                }]
            }]
        }
        headers = {'Content-Type': 'application/json'}
        response = requests.post(url, json=data, headers=headers)
        response.raise_for_status()

        # Extract the text from the response
        text = response.json()['responses'][0]['fullTextAnnotation']['text']
        return text


    def guess_data(self, text: str) -> dict:
        # Extract the named entities using spaCy
        doc = self.nlp(text)
        entities = {ent.label_: ent.text for ent in doc.ents}
        lat, lon = None, None

        top3_libpostal_nominatium = []
        top3_spacy_nominatium = []

        # Extract the address using libpostal
        address = parse_address(text)

        # Extract the phone number
        phone = None
        try:
            for token in doc:
                if token.like_phone:
                    phone = token.text
                    break
        
        except:
            pass


        try:
            address_parts = {}

            # the interesting data are usually at the top of the receipt, just take the first entries
            for feature in address:
                if feature[1] not in address_parts:
                    address_parts[feature[1]] = feature[0]
     

            #build the nominatium query to search for the business in OSM     
            house = address_parts.get('house')
            #remove house from address_parts
            if house:
                address_parts.pop('house')

            query = house + ", " + address_parts["city"]

            # try to query nominatium with the house[business name] and just the city
            resultBroad = self.nominatium_search(query) # Nominatium isn't too smart for strange query string (business name+srl or spa)
            nominiatiumResults = resultBroad
            nominatium = resultBroad

            #check if result is empty
            if(len(resultBroad) == 0): # as last resort just try a search with only the road name and number TODO handle multiple results
                query = address_parts["road"] + " " + address_parts["house_number"] # maybe better to add city too?
                resultBroad = self.nominatium_search(query) 
                nominatium = resultBroad
            if len(resultBroad) > 0: # we found an address with valid latitude and longitude, now try a reverse search, using overpass api
                resultOverpass = self.overpass_search(resultBroad)
                # Iterate through the nodes
                busineessName = []
                nominatium = resultOverpass
                for node in resultOverpass.nodes:
                    busineessName.append(node.tags.get("name"))
    
                # now try to find a place with a sufficiently similar name
                rated_libposta = process.extract(house, busineessName, limit=5, scorer=fuzz.partial_ratio)
                
                if(entities.get('ORG')):
                    rated_spacy = process.extract(entities.get('ORG'), busineessName, limit=5, scorer=fuzz.partial_ratio)

                #get the three most scored results from both libpostal and spacy, using the match score
                top3_libpostal = rated_libposta[:3]
                if(entities.get('ORG')):
                    top3_spacy = rated_spacy[:3]

                #find the corresponding nominatium results for the top 3 results
                for i in range(3):
                    for node in nominatium.nodes:
                        if node.tags.get("name") == top3_libpostal[i][0]:
                            #do a reverse geocode search on nominatium with the lat and lon of the node to get the actual address
                            # this may be not needed as the place is already in osm, so no need to copy/paste the address

                            c_address = self.nominatim.query(node.lat, node.lon, reverse=True, zoom=10)
                            top3_libpostal_nominatium.append({ 'name': node.tags.get("name"), 'lat': node.lat, 'lon': node.lon, 'address': c_address, 'score': top3_libpostal[i][1]})
                     
                        if entities.get('ORG') and node.tags.get("name") == top3_spacy[i][0]:
                            c_address =self.nominatim.query(node.lat, node.lon, reverse=True, zoom=10)
                            top3_spacy_nominatium.append({ 'name': node.tags.get("name"), 'lat': node.lat, 'lon': node.lon, 'address': c_address, 'score': top3_spacy[i][1]})

            if(len(resultBroad)>0):
                lat = float(resultBroad[0]["lat"])
                lon = float(resultBroad[0]["lon"])
        except Exception as e:
            print(e) # add some logs here
            print(traceback.format_exc())

        return {'name': entities.get('ORG'), 'address': address, 'phone': phone, 'coordinates': (lat, lon) , 'top3_libpostal_nominatium': top3_libpostal_nominatium, 'top3_spacy_nominatium': top3_spacy_nominatium}

    def nominatium_search(self, query):
        result = self.nominatim.query(query)
        result = result.toJSON()
        return result

    def overpass_search(self, resultBroad):
        lat = float(resultBroad[0]["lat"])
        lon = float(resultBroad[0]["lon"])
        radius = 1000

        # The overpass query, we may need to add more tags or change the radius
        queryOverpass = f"""
                        [out:json];
                        (
                        node["amenity"~"cafe|shop|restaurant|bar|pub"]({lat-0.01},{lon-0.01},{lat+0.01},{lon+0.01});
                        way["amenity"~"cafe|shop|restaurant|bar|pub"]({lat-0.01},{lon-0.01},{lat+0.01},{lon+0.01});
                        rel["amenity"~"cafe|shop|restaurant|bar|pub"]({lat-0.01},{lon-0.01},{lat+0.01},{lon+0.01});
                        );
                        out;
                    """

        resultOverpass = self.overpassapi.query(queryOverpass)

        return resultOverpass

