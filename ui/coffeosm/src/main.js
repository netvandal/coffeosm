import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'
import './registerServiceWorker'
import { createPinia } from 'pinia'
import VueClipboard from 'vue3-clipboard'

const pinia = createPinia()
const app = createApp(App)

app.use(router)
app.use(pinia)
app.use(VueClipboard, {
    autoSetContainer: true,
    appendToBody: true,
  })
app.mount('#app')
