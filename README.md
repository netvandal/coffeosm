# coffeosm

CoffeeOSM is an app created to simplify the process of adding places to OpenStreetMap (OSM). OSM is a collaborative map of the world that is freely editable, but it can be challenging for newcomers to add new places to the map. CoffeeOSM was designed to make the process easier and more accessible for everyone.

The app is designed to work by allowing users to scan receipts for coffee shops, restaurants, and other locations that they visit. By automatically extracting the relevant information from the receipts, CoffeeOSM makes it easy for users to add new places to the map in just a few clicks.

CoffeeOSM is still in early development stage (more a POC actually).

## Project structure

The backend entry point is located in the coffeosm.py file. It is a small Python API powered by FastAPI that exposes an endpoint called upload_image. The API saves the receipt by removing its EXIF data for privacy, then uses TesseractOCR (with an option for Google Vision API) to extract the receipt text and parses it with the help of Spacy and libpostal. If the place can be found by coordinates or by searching the address, a reverse query is done through Overpass API to retrieve a list of places in the area and score them based on a fuzzy search of the business name. The collected data is then returned in JSON format.

Most of the process should ideally be done on the client side, but to avoid requiring users to install an app and because some operations in JavaScript can be a bit complex, this solution was chosen as a starting point and proof of concept. The UI folder contains a VUE project that provides a simple page for users to upload receipts and call the API. When a response is ready, the page displays the similar OSM places found and the parsed data ready to be copied.

## Running the backend in dev mode
    uvicorn coffeosm:app --reload

## Running the ui in dev mode
    ```sh
    cd ui/coffeosm
    npm install
    npm run dev
    ```

## TODO
Error handling is lacking.. or it didn't exist atm...

## Dependency
apt install python3-opencv python3-pil tesseract-ocr  python3-dev 
https://github.com/openvenues/libpostal
pip3 install uvicorn fastapi overpy fuzzywuzzy python-multipart python-Levenshtein tesseract pytesseract spacy postal OSMPythonTools

python3 -m spacy download en_core_web_sm
## License
Do whatever you want :)git push
