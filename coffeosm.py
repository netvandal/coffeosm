import time
from fastapi import FastAPI, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
import random 
from typing import List
from baseProcessor import BaseProcessor
#from GCVisionProcessor import GCVisionProcessor
import os
from PIL import Image

app = FastAPI()

# CORS enabling
origins = [
     "http://localhost",
     "http://localhost:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/upload_image/")
async def upload_image(file: UploadFile):
    # Create the 'receipts' directory if it doesn't exist
    if not os.path.exists('receipts'):
        os.makedirs('receipts')

    baseProcessor = BaseProcessor()
    #baseProcessor = GCVisionProcessor()

    # Save the file to the 'receipts' directory
    filename = file.filename
    file_path = f'receipts/{filename}'
    
    
    with open(file_path, 'wb') as f:
        f.write(await file.read())

    # remove exif data for privacy
    # it woul be better to do it before the actual upload...    
    image = Image.open(file_path)
    data = list(image.getdata())
    image_without_exif = Image.new(image.mode, image.size)
    image_without_exif.putdata(data)
    #save it with a unique name based on current datetime
    new_name = time.strftime("%Y%m%d-%H%M%S") + "-" + str(random.randint(1,9000))
    extension = filename.split(".")[-1]
    new_name = new_name + "." + extension
    new_path = "receipts/" + new_name
    image_without_exif.save(new_path)
    
    #delete original file
    os.remove(file_path)
    file_path = "receipts/" + new_name
    
    # do the actual processing, and return the extracted data
    data = baseProcessor.extract_data(file_path)
    return {"message": "Image received, processed and data extracted", "data": data}
