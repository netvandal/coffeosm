from abc import ABC, abstractmethod

class ReceiptProcessor(ABC):
    @abstractmethod
    def extract_data(self, image_path: str) -> dict:
        pass

